#!/usr/bin/env python3

import sys
import os
import yaml
import pytz
import pathlib
import traceback
import logging
import argparse

import pprint

from icalendar import Calendar, Event
from datetime import datetime, timedelta

VERSION="0.1"

log_format = "%(levelname)-5s %(module)s:%(funcName)s %(message)s"

DOW = {'mon': 0, 'tue': 1, 'wed': 2, 'thu': 3, 'fri': 4, 'sat': 5, 'sun': 6}
ERRORS = []

def make_array(items):
    result = []
    if type(items) != list:
        return [items]
    else:
        return items

# Inspect the command line
parser = argparse.ArgumentParser(description='Generate a specific calendar')

# standard flag
parser.add_argument('--verbose', '-v', action='store_true',
    help='verbose flag' )
parser.add_argument('--debug', '-d', action='store_true',
    help='debug flag')

# positional argument
parser.add_argument('filename', metavar='FILE',
        help='File to write calendar file to')
args = parser.parse_args()

if args.verbose:
    logging.basicConfig(filename="", format=log_format, level=logging.INFO)
elif args.debug:
    logging.basicConfig(filename="", format=log_format, level=logging.DEBUG)

cal = Calendar()
cal.add('prodid', '-//generate_calendar.py//wt0f.com//%s' % VERSION)

# Read the data from STDIN
net_data = yaml.load(sys.stdin.read(), Loader=yaml.SafeLoader)
logging.debug('Read net data from STDIN')

# We back up about a week to start generating the calendar. This gives
# all the nets for the past week so one could go back in time to find
# a net they missed or heard but was not sure which one it was.
# Maybe this should actually back up a month given that some nets are
# monthly.

# this gives us midnight starting sunday a week ago
weekago = timedelta(days=7)
last_week = datetime.now() - weekago
last_week_start = (last_week - timedelta(days=last_week.weekday()+1,
                                hours=last_week.hour,
                                minutes=last_week.minute,
                                seconds=last_week.second,
                                microseconds=last_week.microsecond)
                  ).replace(tzinfo=pytz.utc)
utcnow = datetime.utcnow().replace(tzinfo=pytz.utc)

for item in net_data:
    net_info = Event()
    logging.info(f"Processing net: {item['name']}")
    # items for events
    #   summary, dtstart, dtend,
    #   location, description
    #   organizer, contact,
    #   organizer;CN=asdf:mailto:email_addr
    net_info.add('summary', item['name'])
    net_info.add('description', item['description'])
    # instruct calendar software not to blockout busy time
    net_info.add('transp', 'OPAQUE')
    if 'location' in item:
        net_info.add('location', item['location'])
    if 'url' in item:
        net_info.add('url', item['url'])
    if 'tags' in item:
        net_info.add('categories', item['tags'])
    if 'status' in item:
        if item['status'] == 'active':
            net_info.add('status', 'CONFIRMED')
        elif item['status'] == 'suspended':
            net_info.add('status', 'CANCELLED')
        else:
            net_info.add('status', 'TENTATIVE')

    # Create the schedule. we start all schedules starting last week
    # to insure that nothing is missed.
    for evt_time in item['schedule']:
        # copy the net definition for each schedule
        event = net_info.copy()

        timezone = pytz.timezone(evt_time['timezone'])
        try:
            start_hour, start_min = [ int(n) for n in evt_time['start_time'].split(':') ]
            end_hour, end_min = [ int(n) for n in evt_time['end_time'].split(':') ]
        except AttributeError:
            # Trying to split an int. This happens when the start_time or
            # end_time are not quoted. The YAML parsers treat the time as
            # an integer instead of a string.
            print("ERROR: time is not quoted for {}".format(item['name']))
            sys.exit(1)

        # get the current time in specified timezone
        now = datetime.now(tz=timezone)

        #
        #  Daily schedules
        #
        if evt_time['frequency'] == 'daily':
            logging.debug('daily schedule')
            start = datetime(now.year, now.month, now.day, start_hour, start_min, tzinfo=timezone) - weekago
            end = datetime(now.year, now.month, now.day, end_hour, end_min, tzinfo=timezone) - weekago

            # check to make sure that we did not roll over midnight
            if start > end:
                try:
                    end = datetime(now.year, now.month, now.day+1, end_hour, end_min, tzinfo=timezone) - weekago
                except ValueError as e:
                    # Day will be invalid if running on the last day of the month
                    if e == 'day is out of range for month':
                        end = datetime(now.year, now.month+1, 1, end_hour, end_min, tzinfo=timezone) - weekago
            event.add('dtstart', start)
            event.add('dtend', end)
            event.add('rrule', {'freq': 'daily'})

        #
        # Weekly schedules
        #
        elif evt_time['frequency'] == 'weekly':
            logging.debug('weekly schedule')
            if not 'day_of_week' in evt_time:
                print("ERROR: day_of_week not specified for weekly schedule for %s" % item['name'], file=sys.stderr)
                exit(1)

            # we only need to look at the first day to set the schedule
            days = make_array(evt_time['day_of_week'])
            if now.weekday() < DOW[days[0]]:
                # earlier in week
                t = now - timedelta(days=now.weekday()-DOW[days[0]])
            else:
                # later in week (or same day)
                t = now + timedelta(days=DOW[days[0]]-now.weekday())

            start = datetime(t.year, t.month, t.day, start_hour, start_min, tzinfo=timezone) - weekago
            end = datetime(t.year, t.month, t.day, end_hour, end_min, tzinfo=timezone) - weekago

            # check to make sure that we did not roll over midnight
            if start > end:
                end = datetime(t.year, t.month, t.day+1, end_hour, end_min, tzinfo=timezone) - weekago

            logging.debug(f"{start=}")
            logging.debug(f"{end=}")
            event.add('dtstart', start)
            event.add('dtend', end)

            # No we define the RRULE that will specify the days of week
            if len(days) > 1:
                event.add('rrule', {'freq': 'weekly', 'byday': list(map(lambda d: d[0:2], days))})
            else:
                # single day so no complex RRULE
                event.add('rrule', {'freq': 'weekly'})

        #
        # Monthly schedules
        #
        elif evt_time['frequency'] == 'monthly':
            logging.debug('monthly schedule')
            if 'which' in evt_time:
                # we start by creating the schedule for the first day of the month
                begin_month = datetime(now.year, now.month, 1, start_hour, start_min, tzinfo=timezone)
                end_month = datetime(now.year, now.month, 1, end_hour, end_min, tzinfo=timezone)

                # check to make sure that we did not roll over midnight
                if begin_month > end_month:
                    end_month = datetime(now.year, now.month, 2, end_hour, end_min, tzinfo=timezone)

                # Then calculate the num of days to the day of the week
                # for the scheduled net
                weekday = make_array(evt_time['day_of_week'])[0]
                days = DOW[weekday] - begin_month.weekday()
                if begin_month.weekday() > DOW[weekday]:
                    days += 7

                # add the times to the calendar entry
                event.add('dtstart', begin_month + timedelta(days=days))
                event.add('dtend', end_month + timedelta(days=days))

                # add an RRULE that specifies specific days of the month
                dayid = weekday[0:2].upper()
                which = [ "%d%s" % (num, dayid) for num in make_array(evt_time['which'])]
                event.add('rrule', {'freq': 'monthly', 'byday': ','.join(which)})
            else:
                # schedule for a day of the month
                start = datetime(now.year, now.month-1, evt_time['day'], start_hour, start_min, tzinfo=timezone)
                end = datetime(now.year, now.month-1, evt_time['day'], end_hour, end_min, tzinfo=timezone)

                # check to make sure that we did not roll over midnight
                if start > end:
                    end = datetime(now.year, now.month-1, evt_time['day']+1, end_hour, end_min, tzinfo=timezone)


                event.add('dtstart', start)
                event.add('dtend', end)
                event.add('rrule', {'freq': 'monthly', 'bymonthday': evt_time['day']})

        # add the event to the calendar
        logging.debug(f"{event=}")
        cal.add_component(event)

# create directories if they do not already exist
pathlib.Path(os.path.dirname(args.filename)).mkdir(parents=True, exist_ok=True)

# write the calendar to a file
with open(args.filename, 'w') as fp:
    fp.write(cal.to_ical().decode('utf-8'))

if len(ERRORS) > 0:
    print("Found the following errors")
    for err in ERRORS:
        print(err)
    sys.exit(1)

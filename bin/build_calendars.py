#!/usr/bin/env python3

import sys
import os
import re
import yaml
import itertools
import argparse
import logging

from subprocess import Popen, PIPE
import pprint

def filter_by_tags(data, tags=[]):
    if not tags: return data

    subdata = list(filter(lambda i: tags[0] in i['tags'], data))
    return filter_by_tags(subdata, tags[1:])

def calendar(items, filename, args):
    cmd = ['bin/generate_calendar.py']
    if args.verbose:
        cmd.append('-v')
    elif args.debug:
        cmd.append('-d')
    cmd.append(filename)

    logging.debug(f'Executing: {" ".join(cmd)}')
    with Popen(cmd, stdin=PIPE) as proc:
        proc.communicate(yaml.dump(items).encode())
        proc.wait()
        if proc.returncode != 0:
            sys.exit(proc.returncode)

log_format = "%(levelname)-5s %(module)s:%(funcName)s %(message)s"

parser = argparse.ArgumentParser(description='Build calendars from radio net database')

# standard flag
parser.add_argument('--verbose', '-v', action='store_true',
    help='verbose flag' )
parser.add_argument('--debug', '-d', action='store_true',
    help='debug flag')

# positional argument
parser.add_argument('tags_file', metavar='TAGSFILE',
        help='File containing calendar generation data')

args = parser.parse_args()

if args.verbose:
    logging.basicConfig(filename="", format=log_format, level=logging.INFO)
elif args.debug:
    logging.basicConfig(filename="", format=log_format, level=logging.DEBUG)

cal_data = yaml.load(open(args.tags_file, 'r').read(), Loader=yaml.SafeLoader)

# structure to record all net information
net_data = []

# read in all the YAML files and add them to net_data
for dirName, subdirList, fileList in os.walk('.', topdown=True):
    # prune dirs that are not necessary
    for d in subdirList.copy():
        if re.search("\.git|lib|bin", d):
            subdirList.remove(d)

    # process each of the YAML files containing calendar data
    for fname in filter(lambda f: f.endswith('.yaml'), fileList):
        # ignore the tags file used for configuration
        if fname == args.tags_file:
            break

        logging.info(f'Processing: {dirName}/{fname}')
        with open('%s/%s' % (dirName, fname), 'r') as f:
            file_data = yaml.load(f.read(), Loader=yaml.SafeLoader)

            # if the YAML file has top level tags of
            # tags or timezone, then apply them to all
            # entries contained in the YAML file.
            if 'tags' in file_data:
                file_tags = file_data['tags']
            else:
                file_tags = []

            if 'timezone' in file_data:
                file_timezone = file_data['timezone']
            else:
                file_timezone = None

            # process each net applying the tags and timezone info
            for net_info in file_data['nets']:
                # Mix in tags defined at the file level
                if 'tags' in net_info:
                    net_info['tags'] = list(itertools.chain(net_info['tags'], file_tags))
                else:
                    net_info['tags'] = file_tags

                # set the time zone if not overridden
                for sched in net_info['schedule']:
                    if not 'timezone' in sched and file_timezone:
                        sched['timezone'] = file_timezone
                    elif not 'timezone' in sched:
                        raise LookupError('No timezone defined for %s in %s/%s' % (net_info['name'], dirName, fname))

                net_data.append(net_info)

for cal in cal_data:
    calendar(filter_by_tags(net_data, cal_data[cal]['tags']), 'calendars/%s' % cal, args)

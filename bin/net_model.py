
class ModelError < StandardError: pass


class Frequency:
    modes = ('FM', 'LSB', 'USB', 'AM', 'CW')

    def __init__(self, freq=None, mode=None):
        self.frequency = freq
        if not defined(mode):
            self.mode = None
        elif mode in Frequency.modes:
            self.mode = mode
        else:
            raise ModelError, "Mode (%s) is not recognized".format(mode)

    def __repr__(self):
        return "%s %s".format(self.frequency, self.mode)

class Schedule:
    day_of_week = ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat')

    def __init__(self):
        pass

class NetModel:

    def __init__(self):
        self.name         = None
        self.tags         = []
        self.schedule     = None
        self.last_updated = None
        self.url          = None
        self.frequency    = None
        self.talkgroup    = None
        self.contact      = None
        self.description  = None


    def create_from_dict(self, data):
        self.name = data.name
        self.tags.append(data.tags)
        self.schedule = Schedule(data.schedule)
        self.url = data.url
        self.frequency = Frequency(data.frequency)
        self.talkgroup = data.talkgroup
        self.contact = data.contact
        self.description = data.description




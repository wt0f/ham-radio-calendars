# Ham Radio Calendars

[![pipeline status](https://gitlab.com/wt0f/ham-radio-calendars/badges/master/pipeline.svg)](https://gitlab.com/wt0f/ham-radio-calendars/-/commits/master)

Files in this repository contain schedules of various ham radio nets and
produces calendar files according to a number of tags added to each entry.

## Downloading calendars

A list of available calendars can be found at

> https://nexus.wt0f.com/service/rest/repository/browse/calendars/

Individual calendars can be accessed with the following URL pattern:

> https://nexus.wt0f.com/repository/calendars/ICS_FILE

Nearly any calendar program will allow you to specify the calendar URL to
subscribe to the calendar. As a result, as the calendar is modified with
newer net definitions, the calendar program will update the schedules and
display the modified schedules.

## Submitting Changes

Anyone is welcome to update and correct any information contained in this
repository. This is done through the process of forking the repository to
your account, making changes and then creating a merge request (MR) back to
the original repository. The MR is then reviewed to insure that the change
is correct and adheres to the standard formats allow. Once the MR is
approved, it will be merged into the repository and the calendars will
automatically be rebuilt.

## Tags

### Geographic tags

Most net definition files should include geographic tags so that nets can
be categorized by a region and compiled into a calendar.

- `geo:city:<>`
- `geo:country:<>`
- `geo:state:<>`
- `geo:province:<>`
- `geo:county:<>`
- `geo:arrl:section:<>`
- `geo:arrl:division:<>`

Not all geo tags may be specified and mostly depend on the location of the
nets. In addition, there is an older tag that is still in use which will
transition to `geo:region:<>`.

- `location:<>`

Allowed ARRL section names: AL, AK, AZ, EB, LAX, ORG, SV, SDG, SF, SJV, SB,
SCV, PAC, CO, CT, DE, NFL, SFL, WCF, GA, HI, ID, IL, IN, IA, KS, KY, LA, ME,
MDC, EMA, WMA, MI, MN, MS, MO, MT, NE, NV, NH, NNJ, SNJ, NM, NLI, NNY, ENY,
WNY, NC, ND, OH, OK, OR, EPA, WPA, PR, RI, SC, SD, TN, NTX, STX, WTX, VI, UT,
VT, VA, EWA, WWA, WV, WI, WY, MAR, NL, PE, QC, ONE, ONN, ONS, GTA, MB, SK, AB,
BC, NT

Allowed ARRL division names: Atlantic, Central, Dakota, Delta, Great_Lakes,
Hudson, Midwest, New_England, Northwest, Pacific, Roanoke, Rocky_Mountain,
Southeastern, Southwestern, West_Gulf

### Mode tags

Tags are also defined for the mode of transmission.

- `phone`
- `cw`
- `digital`
- `winlink`
- `DMR`
- `DSTAR`
- `FUSION`

### Net styles

- `EMCOMM`
- `maritime`
- `weather`
- `checkin`
- `wellness`

If you find that there is another net style that is not listed here, please
[create an issue](https://gitlab.com/wt0f/ham-radio-calendars/-/issues/new?issuable_template=Add%20Net%20Style)
 in the GitLab repository to request that the style be added.

## Schema

Each radio net gets listed in a YAML file that is categorized by geographic
region or organization. Each file is structured as follows:

```yaml
---
timzone: America/Los_Angeles
tags:
  - CW
nets:
  - name: The net
    ...
```

The `timezone` and `tags` entries are optional. When specified they are
applied to each net definition in the file. In the case of `timezone`, it
provides the default timezone for a net if not specified in the net
definition. This is convenient for files that contain nets organized by
a geographic region.

When `tags` are defined at the top level, the defined tags are merged with
any tags that are defined at the net definition level. There is no way to
exclude tags from a definition so tags defined at the top level should be
a superset of tags for all defined nets.

The following is a typical definition of a net:

```yaml
nets:
  - name: Colorado Daily WX Net
    status: (unknown, active, suspended)
    tags:
      - Phone
      - WX
    schedule:
      - frequency: daily
        every: 2
        start_time: "06:15"
        end_time: "06:45"
        timezone: America/Los_Angeles
      - frequency: weekly
        day_of_week:
          - (sun, mon, tue, wed, thu, fri, sat)
        every: 2
        start_time: "06:15"
        end_time: "06:45"
        timezone: America/Los_Angeles
      - frequency: monthly
        day: 13
        every: 2
        start_time: "06:15"
        end_time: "06:45"
        timezone: America/Los_Angeles
      - frequency: monthly
        day_of_week: wed
        which: (1, 2, 3, 4, 5)
        start_time: "06:15"
        end_time: "06:45"
        timezone: America/Los_Angeles
    url: ""
    frequency: ""
    talkgroup: 310815
    contact: ""
    description: -|
```

**Note**: all time references need to be quoted as they contain colons
which breaks the import of the YAML file.

This example shows all the variations for defining a schedule. Any number
of schedules can be specified and should be flexible enough for any crazy
schedule.

The following attributes are not fully supported at this time and are
reserved for future development.

- `status`
- `schedule.[].every`
- `frequency.[]`
- `repeater.[]`
- `talkgroup`
- `node.{allstar,echolink,reflector,...}`

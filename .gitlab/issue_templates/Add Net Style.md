<!-- Fill in as much as possible. If there are questions or more information
needed, comments will be added to this issue. -->

## Information

- Net style to add: `style_name`

## Description

Please describe the net style that you are requesting and please include
information about how the net style is different than the currently defined
net styles in the [README file](https://gitlab.com/wt0f/ham-radio-calendars/-/blob/master/README.md).



/label ~feature

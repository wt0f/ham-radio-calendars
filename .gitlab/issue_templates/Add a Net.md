<!-- Fill in as much as possible. If there are questions or more information
needed, comments will be added to this issue. -->

## Information

- Name:
- Org/club:
- City:
- State:
- Country:

## Description

- URL reference:
- Description:

## Frequency

- [ ] Repeater: <Callsign or name>
- [ ] Simplex
- [ ] Echolink node:
- [ ] Allstar node:
- [ ] Reflector:
- [ ] Other: <Describe how to reach the net>

Frequencies:

<!-- Specify frequency in MHz, offset (+/- for standard offsets or +/- followed
by KHz offset) and PL tone in Hz. Examples: 147.25-/123.0, 53.68 (simplex, no tone),
443.450+6000/203.5 -->

-

### Mode

- [ ] Analog/FM
- [ ] USB
- [ ] LSB
- [ ] CW
- [ ] D*Star
- [ ] DMR
- [ ] Fusion
- [ ] Winlink
- [ ] Other: <specify mode>

## Schedule

- [ ] Daily
- [ ] Weekly
- [ ] Monthly

Day of week:
Start time:
End Time:

Exceptions: